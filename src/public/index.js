/**
 * Returns Array of Object of given data.
 * @param {Object} matchData
 */

function makeSeriesData(matchData) {
  const seriesData = Object.keys(matchData).reduce((totalMatch, year) => {
    let matchObj = {};
    matchObj["name"] = year;
    matchObj["y"] = matchData[year];
    totalMatch.push(matchObj);
    return totalMatch;
  }, []);
  return seriesData;
}

fetch("./matchesPerYearData.json")
  .then((res) => res.json())
  .then((matchData) => {
    const seriesData = makeSeriesData(matchData);

    Highcharts.chart("container1", {
      chart: {
        type: "column",
      },
      title: {
        text: "Total Matches Conducted Per Year in the IPL",
      },
      subtitle: {
        text: "",
      },
      accessibility: {
        announceNewData: {
          enabled: true,
        },
      },
      xAxis: {
        type: "category",
        title: {
          text: "Year",
        },
      },
      yAxis: {
        title: {
          text: "Matches Per Year",
        },
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },

      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat:
          '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> matches<br/>',
      },

      series: [
        {
          name: "Matches",
          colorByPoint: true,
          data: seriesData,
        },
      ],
    });
  })
  .catch((err) => console.log(err));

fetch("./economicalBowlersData.json")
  .then((res) => res.json())
  .then((matchData) => {
    const seriesData = makeSeriesData(matchData);

    Highcharts.chart("container2", {
      chart: {
        type: "column",
      },
      title: {
        text: "Most Economical Bowlers of the year 2015",
      },
      subtitle: {
        text: "<small>(Lower is better)</small>",
      },
      accessibility: {
        announceNewData: {
          enabled: true,
        },
      },
      xAxis: {
        type: "category",
        title: {
          text: "Players",
        },
      },
      yAxis: {
        title: {
          text: "Economy Rate",
        },
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y:.2f}",
          },
        },
      },

      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat:
          '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>',
      },

      series: [
        {
          name: "Bowler",
          colorByPoint: true,
          data: seriesData,
        },
      ],
    });
  })
  .catch((err) => console.log(err));

fetch("./extraRunsPerTeamData.json")
  .then((res) => res.json())
  .then((matchData) => {
    const seriesData = makeSeriesData(matchData);

    Highcharts.chart("container3", {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie",
      },
      title: {
        text: "Extra Runs Per Team in the Year 2016",
      },
      subtitle: {
        text: "<small>(Lower is Better)</small>",
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.y} runs</b>",
      },
      accessibility: {
        point: {
          valueSuffix: "",
        },
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.y} runs",
          },
        },
      },
      series: [
        {
          name: "Team",
          colorByPoint: true,
          data: seriesData,
        },
      ],
    });
  })
  .catch((err) => console.log(err));

fetch("./matchesWonPerTeamPerYearData.json")
  .then((res) => res.json())
  .then((matchData) => {
    const totalWins = [];
    const seasons = [];
    matchData.forEach((match) => {
      for (let data in match) {
        let tempMatchObj = {};
        if (data === "season") seasons.push(match[data]);
        else if (totalWins.find((tempMatchObj) => tempMatchObj.name === data)) {
          totalWins.find((tempMatchObj) => {
            if (tempMatchObj.name === data) {
              tempMatchObj.y += match[data];
            }
          });
        } else if (data !== "" || undefined) {
          tempMatchObj["name"] = data;
          tempMatchObj["y"] = match[data];
          tempMatchObj["drilldown"] = data;
          totalWins.push(tempMatchObj);
        }
      }
    });

    const drilldown = [];

    matchData.forEach((match) => {
      for (let data in match) {
        let tempMatchObj = {};
        if (drilldown.find((tempMatchObj) => tempMatchObj.name === data)) {
          drilldown.find((tempMatchObj) => {
            if (tempMatchObj.name === data)
              tempMatchObj.data.push([match["season"], match[data]]);
          });
        } else if ((data !== "" || undefined) && data !== "season") {
          tempMatchObj["name"] = data;
          tempMatchObj["id"] = data;
          tempMatchObj["data"] = [[match.season, match[data]]];
          drilldown.push(tempMatchObj);
        }
      }
    });

    Highcharts.chart("container4", {
      chart: {
        type: "column",
      },
      title: {
        text: "Matches Won Per Team Per Year",
      },
      subtitle: {
        text: "",
      },
      accessibility: {
        announceNewData: {
          enabled: true,
        },
      },
      xAxis: {
        type: "category",
      },
      yAxis: {
        title: {
          text: "Total Matches Won",
        },
      },
      legend: {
        enabled: false,
      },
      plotOptions: {
        series: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            format: "{point.y}",
          },
        },
      },

      tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat:
          '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> matches won<br/>',
      },

      series: [
        {
          name: "Browsers",
          colorByPoint: true,
          data: totalWins,
        },
      ],
      drilldown: {
        series: drilldown,
      },
    });
  })
  .catch((err) => console.log(err));
