const { convertCsvToJson, teamDataPerYear, writeData } = require("./utils");
const economicalBowlers = require("./ipl_modules/economicalBowlers");
const matchesPerYear = require("./ipl_modules/matchesPerYear");
const matchesWonPerTeamPerYear = require("./ipl_modules/matchesWonPerTeamPerYear");
const extraRunsPerTeam = require("./ipl_modules/extraRunsPerTeam");

async function jsonDataOutput() {
  try {
    const matchesData = await convertCsvToJson("../data/matches.csv");
    const deliveriesData = await convertCsvToJson("../data/deliveries.csv");
    const teamData2016 = teamDataPerYear(matchesData, deliveriesData, "2016");
    const teamData2015 = teamDataPerYear(matchesData, deliveriesData, "2015");

    const matchesPerYearData = matchesPerYear(matchesData);
    const matchesWonPerTeamPerYearData = matchesWonPerTeamPerYear(matchesData);
    const extraRunsPerTeamData = extraRunsPerTeam(teamData2016);
    const economicalBowlersData = economicalBowlers(teamData2015);

    writeData(matchesPerYearData, "../public/output/matchesPerYearData.json");
    writeData(
      matchesWonPerTeamPerYearData,
      "../public/output/matchesWonPerTeamPerYearData.json"
    );
    writeData(
      extraRunsPerTeamData,
      "../public/output/extraRunsPerTeamData.json"
    );
    writeData(
      economicalBowlersData,
      "../public/output/economicalBowlersData.json"
    );
  } catch (err) {
    console.error(err);
  }
}

jsonDataOutput();
