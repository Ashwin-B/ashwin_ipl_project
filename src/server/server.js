const fs = require("fs");
const http = require("http");
const path = require("path");

/**
 * Returns the read data.
 * @param {string} filenamePath
 */

function readData(filenamePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(path.resolve(__dirname, filenamePath), "utf8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

/**
 *Writes the response if error occours.
 * @param {Object} error
 * @param {http.ServerResponse} res
 */

function errorHandler(error, res) {
  if (error) {
    res.writeHead(404, { "Content-Type": "text/html" });
    res.write("<h1>File not Found</h1>");
    res.end();
  }
}

const server = http.createServer((req, res) => {
  switch (req.url) {
    case "/":
      readData("../public/index.html")
        .then((data) => {
          res.writeHead(200, { "Content-Type": "text/html" });
          res.write(data);
          res.end();
        })
        .catch((err) => errorHandler(err, res));
      break;

    case "/style.css":
      readData("../public/style.css")
        .then((data) => {
          res.writeHead(200, { "Content-Type": "text/css" });
          res.write(data);
          res.end();
        })
        .catch((err) => errorHandler(err, res));
      break;

    case "/index.js":
      readData("../public/index.js")
        .then((data) => {
          res.writeHead(200, { "Content-Type": "text/javascript" });
          res.write(data);
          res.end();
        })
        .catch((err) => errorHandler(err, res));
      break;

    case "/matchesPerYearData.json":
      readData("../public/output/matchesPerYearData.json")
        .then((data) => {
          res.writeHead(200, { "Content-Type": "application/json" });
          res.write(data);
          res.end();
        })
        .catch((err) => errorHandler(err, res));
      break;

    case "/matchesWonPerTeamPerYearData.json":
      readData("../public/output/matchesWonPerTeamPerYearData.json")
        .then((data) => {
          res.writeHead(200, { "Content-Type": "application/json" });
          res.write(data);
          res.end();
        })
        .catch((err) => errorHandler(err, res));
      break;

    case "/extraRunsPerTeamData.json":
      readData("../public/output/extraRunsPerTeamData.json")
        .then((data) => {
          res.writeHead(200, { "Content-Type": "application/json" });
          res.write(data);
          res.end();
        })
        .catch((err) => errorHandler(err, res));
      break;

    case "/economicalBowlersData.json":
      readData("../public/output/economicalBowlersData.json")
        .then((data) => {
          res.writeHead(200, { "Content-Type": "application/json" });
          res.write(data);
          res.end();
        })
        .catch((err) => errorHandler(err, res));
      break;

    default:
      res.writeHead(404, { "Content-Type": "text/html" });
      res.write("<h2>No Page Found</h2>");
      res.end();
  }
});

server.listen(8000);
