/**
 * Returns Extra Runs Per Team Object.
 * @param {Array.<Object>} teamData
 */

function extraRunsPerTeam(teamData) {
  const extraRunsPerTeamResult = teamData.reduce((totalTeamRuns, team) => {
    let extraRuns = parseInt(team.extra_runs);
    let battingTeam = team.batting_team;
    if (battingTeam in totalTeamRuns) {
      totalTeamRuns[battingTeam] += extraRuns;
    } else {
      totalTeamRuns[battingTeam] = extraRuns;
    }

    return totalTeamRuns;
  }, {});

  return extraRunsPerTeamResult;
}

module.exports = extraRunsPerTeam;
