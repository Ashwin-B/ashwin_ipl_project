/**
 *Returns Matches Per Year Object.
 * @param {Array.<Object>} matchesData
 */

function matchesPerYear(matchesData) {
  const matchesPerYear = matchesData.reduce((totalMatches, match) => {
    if (match.season in totalMatches) {
      totalMatches[match.season] += 1;
    } else if (match.season !== undefined || !match.season) {
      totalMatches[match.season] = 1;
    }

    return totalMatches;
  }, {});

  return matchesPerYear;
}

module.exports = matchesPerYear;
