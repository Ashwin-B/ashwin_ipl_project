/**
 * Returns top ten economical bowlers Object.
 * @param {Array.<Object>} teamData
 */

function economicalBowlers(teamData) {
  const totalBalls = {};
  const totalRunsByBowlers = {};

  teamData.forEach((team) => {
    {
      let bowler = team.bowler;
      let totalRuns = parseInt(team.total_runs);
      if (!(bowler in totalBalls)) {
        totalRunsByBowlers[bowler] = totalRuns;
        totalBalls[bowler] = 1;
      } else {
        totalRunsByBowlers[bowler] += totalRuns;
        totalBalls[bowler] += 1;
      }
    }
  });

  const economyRate = Object.keys(totalBalls).reduce((economyRate, bowlers) => {
    let overs = totalBalls[bowlers] / 6;
    economyRate[bowlers] = totalRunsByBowlers[bowlers] / overs;
    return economyRate;
  }, {});

  let economyRateArray = Object.entries(economyRate);
  const topTenEconomyRateArray = economyRateArray
    //a[1] and b[1] represents the economyRate of the bowlers
    .sort((a, b) => a[1] - b[1])
    .slice(0, 10);

  const topTenEconomyRate = topTenEconomyRateArray.reduce(
    (economyObj, bowler) => {
      //bowler[0] represent the bowlers and bowler[1] represent economyRate
      economyObj[bowler[0]] = bowler[1];
      return economyObj;
    },
    {}
  );

  return topTenEconomyRate;
}

module.exports = economicalBowlers;
