/**
 * Returns Matches Won Per Year Array.
 * @param {Array.<Object>} matchData
 */

function matchesWonPerTeamPerYear(matchData) {
  let tempMatchObj = {};

  const matchesWonPerTeam = matchData.reduce((totalMatchWon, match) => {
    if (tempMatchObj.season === match.season) {
      if (match.winner in tempMatchObj) {
        tempMatchObj[match.winner] += 1;
      } else if (Object.values(match.winner).length !== 0) {
        tempMatchObj[match.winner] = 1;
      }
    } else if (
      tempMatchObj.season !== match.season &&
      Object.keys(tempMatchObj).length !== 0
    ) {
      totalMatchWon.push(tempMatchObj);
      tempMatchObj = {};
    }

    if (!(match.season in tempMatchObj)) {
      tempMatchObj.season = match.season;
    }

    return totalMatchWon;
  }, []);

  return matchesWonPerTeam;
}

module.exports = matchesWonPerTeamPerYear;
