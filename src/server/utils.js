const fs = require("fs");
const path = require("path");
const csvtojson = require("csvtojson");

/**
 *Converts CSV files to JSON.
 * @param {string} filePath
 */

function convertCsvToJson(filePath) {
  return new Promise((resolve, reject) => {
    csvtojson()
      .fromFile(path.resolve(__dirname, filePath))
      .then((jsonData) => resolve(jsonData))
      .catch((err) => reject(err));
  });
}

/**
 *Returns team data required for the required year as Array of Objects.
 * @param {Array.<Object>} matches
 * @param {Array.<Object>} deliveries
 * @param {string} year
 * matches and deliveries takes Array of Objects as an argument.
 */

function teamDataPerYear(matches, deliveries, year) {
  const teamData = [];
  matches.forEach((match) => {
    deliveries.find((delivery) => {
      if (match.season === year && match.id === delivery.match_id) {
        teamData.push(delivery);
      }
    });
  });
  return teamData;
}

/**
 *Dumps JSON files to the output folder.
 * @param {any} data
 * @param {string} pathName
 */

function writeData(data, pathName) {
  json = JSON.stringify(data);
  fs.writeFile(path.resolve(__dirname, pathName), json, (err) => {
    if (err) {
      console.log(err);
    }
  });
}

module.exports = { convertCsvToJson, writeData, teamDataPerYear };
